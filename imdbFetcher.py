import urllib2
import os
import time
import sys
from threading import Thread
from bs4 import BeautifulSoup

def fetchPages(start, end):
	#var = 0
	dirName = "imdbPages/"
	curr = time.clock()

	for x in range(start, end +  1):
		#curr = time.clock() - curr
		#print curr
		url = 'http://www.imdb.com/title/tt' + str(x) + '/'

		user_agent = "Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.152 Safari/537.22"
		headers = {'User-Agent' : user_agent}
	
		try:
		    req = urllib2.Request(url)
		    response = urllib2.urlopen(req)
		except urllib2.HTTPError, e:
		    pass
		else:
		    #curr = time.clock() - curr
		    #print curr
		    cnt = 0
		    while(1):
		    	    page = None
			    try:
			    	page = response.read()
			    except:
			    	if(cnt >= 5):
			    		break
			    	time.sleep(1)
			    	cnt += 1
			    else:
				    page = os.linesep.join([s for s in page.splitlines() if s])
				    #os.write(1, "".join(page))>>f1
				    #curr = time.clock() - curr
				    #print curr
				    #var += 1
				    fileName = "./" + dirName + str(x) + ".txt"
				    f1 = open(fileName,"w");
				    print>>f1, page
				    #curr = time.clock() - curr
				    #print curr
				    #print
				    break
	
def parsePages(fileName, f1):
		try:
		    page = open(fileName,"r")
		except:
		    return
		parsed_html = BeautifulSoup(page,"lxml")

		reqDiv = parsed_html.find_all("div",{"class":"maindetails_center"})
		
		for t in reqDiv:
		    allImg = t.find_all("img")
		    if(len(allImg) > 0):
			print>>f1, allImg[0].get('src')
		    
		    movieName = t.find_all("span",{"class":"itemprop"})
		    if(len(movieName) > 0):
		        print>>f1, movieName[0].get_text().encode('utf-8')
		    
		    movieYearH1 = t.find_all("h1",{"class":"header"})
		    if(len(movieYearH1) > 0):
		        movieYear = movieYearH1[0].find_all("span",{"class":"nobr"})
		        if(len(movieYear) > 0):
		            print>>f1, movieYear[0].get_text().encode('utf-8')
		    
		    desc = t.find_all("p",{"itemprop":"description"})
		    if(len(desc) > 0):
		        print>>f1, desc[0].get_text().encode('utf-8')
		        
		    director = t.find_all("div",{"class":"txt-block"})
		    if(len(director) > 0):
		    	#director = directorDiv.find_all("span",{"itemprop":"name"})
		    	print>>f1, director[0].get_text().encode('utf-8')
		    
		    print>>f1, '10**********10==========10++++++++++\n'
		    
		    break
		   
		   
#main()
 
#Fetching

#st = int(sys.argv[1])
#ed = int(sys.argv[2])
#print st,ed

numThreads = 100
st = 20701
lim = 100
incr = 101
threads = []
for y in range(0,numThreads):
    t = Thread(target = fetchPages, args = (st, st + lim,) )
    try:
    	t.start()
    	threads.append(t)
    	st += incr
    except:
    	print "Unable to start thread\n"

"""    	
print "Waiting...."

for t in threads:
    t.join()

print "Done..."
"""

#Parsing

"""
#var = 1
out = open("./imdbPages/parsedOutput.txt", "w")
for x in range(st, ed + 1):
    inp = "./imdbPages/" + str(x) + '.txt'
    parsePages(inp.encode('utf-8'), out)
    #var += 1
"""  
    
